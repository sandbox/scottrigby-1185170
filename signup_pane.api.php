<?php

/**
 * @file
 * This file documents the hooks invoked by the Signup pane module.
 */

/**
 * Hook to define panes available to insert into the signup form.
 *
 * Panes should be provided by callback functions as a FormAPI array.
 * The callback should have the following signature:
 *   function my_callback(&$signup_form, &$form_state, $node, $signup, $pane_id, $signup_type = 'auth')
 * See signup_basic_form_form for an example.
 * The values submitted to the form elements defined by this form will be 
 * serialized and stored in the {signup_log} table as 'form_data'.
 *
 * @param $node
 *  (optional) The node being considered for panes.
 *  Most modules won't need to look at this, but you may need to only return
 *  panes if the node satisfies certain properties.
 *
 * @return
 *   An array of possible forms, keyed by a unique ID. Each value is itself an 
 *   array of data, with the following key-value pairs:
 *     - 'label': (required) The human-readable name of the form.
 *     - 'description': (required) Extra information about the form.
 *     - 'callback': (required) The name of a function.
 *     - 'operations': (optional) A list of links for the user to perform 
 *        administrative tasks on this pane, either global or per-node settings.
 *        The format is an unkeyed array of link arrays (suitable for passing
 *        to theme_links).
 *        You may use %nid as a token in your link's href property to
 *        insert the node id of the current signup node. 
 *        The following optional keys are also available:
 *        - destination: if set to TRUE, the return destination will be appended
 *          to the link as a query string with drupal_get_destination, allowing
 *          the user to return to this page directly. Do not use if your link
 *          sends the user to a complex series of forms or pages.
 *        - not_defaults: if set to TRUE, this link will not be shown when 
 *          default signup settings are being edited at admin/settings/signup.
 *          Use this when your settings link would be meaningless in this 
 *          context because it is dependent on the current node.
 *
 * @see signup_basic_form_form.
 */
function hook_signup_pane_info($node = NULL) {
  return array(
    'basic' => array(
      'label' => 'Basic form',
      'description' => 'Collects name and phone number.',
      'callback' => 'signup_basic_form_form',
    ),  
  );
}

/**
 * Hook to allow modules to alter signup form data prior to displaying signup
 * records in, for example, a node's list of signups at node/N/signups/admin.
 *
 * This allows modules that implement signup panes for format or even inject 
 * their own data.
 *
 * @param $form_data
 *  The user's signup data to alter.
 * @param $nid
 *  The node id for the signup-enabled node.
 * @param $sid
 *  The signup record id.
 * @param $uid
 *  The user id whose signup this is; 0 if this is an anonymous signup.
 * @param $type
 *  The type of output being prepared. Possible values are:
 *    - 'list': The hardcoded admin lists of signups, eg at node/X/signups/admin
 *    - 'view': The form data field in Views.
 *    - 'mail': Email output. This is likely the only one that needs special 
 *      handling; in this case, modules should be more generous about supplying
 *      data since there's no other place to see it.
 */
function hook_signup_form_data_display_alter(&$form_data, $nid, $sid, $uid, $type = 'list') {
  foreach ($form_data as $pane_id => $pane_data) {
    // If this is one of your panes, do stuff to it.
  }
}
